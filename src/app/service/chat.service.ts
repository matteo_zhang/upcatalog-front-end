import { Injectable } from '@angular/core';

// Declare SockJS and Stomp
declare var SockJS;
declare var Stomp;

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  // public stompClient;
  // public msg = [];

  webSocket: WebSocket;
  public messages: string[] = [];

  constructor() { } 
  
  openConnection() {
    this.webSocket = new WebSocket('ws://192.168.1.6:8080/chat')

    this.webSocket.onopen = (event) => {
      console.log('Open: ', event);
    }

    this.webSocket.onmessage = (event) => {
      let incomingMessage = event.data;
      this.messages.push(incomingMessage);

      // console.log('')
    }

    this.webSocket.onclose = (event) => {
      console.log('Close: ', event);
    }
  }

  sendMessage(message: string) {
    this.webSocket.send(message);
  }

  closeWebSocket() {
    this.webSocket.close();
  }
}

// old version
/*
  import { Injectable } from '@angular/core';

// Declare SockJS and Stomp
declare var SockJS;
declare var Stomp;

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  public stompClient;
  public msg = [];

  constructor() {
    this.initializeWebSocketConnection();
  }

  initializeWebSocketConnection() {
    const serverUrl = 'http://localhost:8080/socket';
    let ws = new SockJS(serverUrl);

    this.stompClient = Stomp.over(ws);
    const that = this;
    // tslint:disable-next-line:only-arrow-functions
    this.stompClient.connect({}, function(frame) {
      // '/message' is the broker name of the backend
      that.stompClient.subscribe('/user/queue/greetings', (message) => {
        if (message.body) {
          that.msg.push(message.body);
          console.log(that.msg);
        }
      });
    });
  }

  sendMessage(message) {
    this.stompClient.send('/app/greetings' , {}, message);
  }
}

*/

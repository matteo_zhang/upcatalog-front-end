import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Company } from '../model/Company';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  upCatalogUrl = environment.upCatalogApiUrl + '/customer/company';

  constructor(private http: HttpClient) { }

  getCompanyUserList(): Observable<Company[]> {
    return this.http.get<Company[]>(this.upCatalogUrl);
  }

  addCompanyUser(item: Company): Observable<Company> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })};
    
    return this.http.post<Company>(this.upCatalogUrl, item, httpOptions);
  }  
}

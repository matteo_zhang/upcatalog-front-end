import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { Product } from '../model/Product';
import { Category } from '../model/Category';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  upCatalogUrl = environment.upCatalogApiUrl;

  private categoryListSource = new Subject<Category[]>();
  categoryList = this.categoryListSource.asObservable();

  constructor(private http: HttpClient) { }

  sendCategoryList(categoryList: Category[]) {
    return this.categoryListSource.next(categoryList);
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.upCatalogUrl + '/product');
  }
}

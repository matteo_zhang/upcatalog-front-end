import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TokenStorageService } from '../auth/service/token-storage.service';
import { CartElement } from '../model/CartElement';
import { CustomerUser } from '../model/CustomerUser';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  apiUrl = environment.upCatalogApiUrl + '/cart';

  cart: CartElement[] = new Array();

  constructor(private http: HttpClient,
              private tokenStorageService: TokenStorageService) { }

  getCartList() {
    const customerId = this.tokenStorageService.getUser().id;

    return this.http.get<CartElement[]>(this.apiUrl + '/' + customerId);
  }

  addCartElement(el: CartElement) {
    // if cart is empty try to populate
    this.initCart();

    let foundCart;

    this.cart.forEach(function(item) {
      if (item.product.id === el.product.id) {
        foundCart = item;
      }
    });

    console.log("cart service", foundCart);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })};

    if (foundCart === undefined) {
      this.cart.push(el);
      el.customerUser = this.tokenStorageService.getUser() as CustomerUser;
      console.log("add")

      return this.http.post<CartElement>(this.apiUrl, el, httpOptions);
    }
    else {
      console.log("change")
      return this.changeQuantity(foundCart, el.quantity);
    }
  }

  findCart(cart, id) {
    return cart.product.id === id;
  }

  changeQuantity(el: CartElement, quantity: number) {
    // if cart is empty try to populate
    this.initCart();

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })};

    el.quantity += quantity;

    return this.http.put<CartElement>(this.apiUrl, el, httpOptions);
  }

  emptyCart() {
    return this.http.delete<CartElement>(this.apiUrl);
  }

  initCart() {
    if (this.cart.length === 0) {
      this.getCartList().subscribe(data => {
        this.cart = data;
      });
    }
  }
}

import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Role } from '../model/Role';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  apiUrl = environment.upCatalogApiUrl + '/role';

  constructor(private http: HttpClient) { }

  getRoleList(): Observable<Role[]> {
    return this.http.get<Role[]>(this.apiUrl);
  }
}

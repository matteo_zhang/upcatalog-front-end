import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Supplier } from '../model/Supplier';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {
  upCatalogUrl = environment.upCatalogApiUrl;

  constructor(private http: HttpClient) { }

  getSupplier() {
    return this.http.get<Supplier[]>(this.upCatalogUrl + '/supplier');
  }
}

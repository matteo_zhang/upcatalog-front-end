import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CustomerUser } from '../model/CustomerUser';

@Injectable({
  providedIn: 'root'
})
export class CustomerUserService {
  upCatalogUrl = environment.upCatalogApiUrl + '/customer';

  constructor(private http: HttpClient) { }

  getCustomerUsers() {
    return this.http.get<CustomerUser[]>( this.upCatalogUrl);
  }

  getCustomerUserById(id: number) {
    return this.http.get<CustomerUser>(this.upCatalogUrl + '/' + id);
  }

  updateCustomerUser(item: CustomerUser) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })};
      return this.http.put<CustomerUser>(this.upCatalogUrl, item, httpOptions);
  }
}

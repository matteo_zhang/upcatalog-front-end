import { TestBed } from '@angular/core/testing';

import { CustomerUserService } from './customer-user.service';

describe('CustomeruserserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomerUserService = TestBed.get(CustomerUserService);
    expect(service).toBeTruthy();
  });
});

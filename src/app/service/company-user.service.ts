import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TokenStorageService } from '../auth/service/token-storage.service';
import { CompanyUser } from '../model/CompanyUser';
import { RoleService } from './role.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyUserService {
  upCatalogUrl = environment.upCatalogApiUrl + '/company';

  // TODO: use role from database.
  private roles = ['ROLE_EMPLOYEE', 'ROLE_MANAGER', 'ROLE_ADMIN'];

  constructor(private http: HttpClient,
              private tokenStorageService: TokenStorageService,
              private roleService: RoleService
            ) { }

  getCompanyUserList(): Observable<CompanyUser[]> {
    return this.http.get<CompanyUser[]>(this.upCatalogUrl);
  }

  findCompanyUserById(id: number) {
    return this.http.get<CompanyUser>(this.upCatalogUrl + '/' + id);
  }

  addEmployeeUser(item: CompanyUser) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })};

    const upCatalogRegisterUrl = environment.upCatalogApiUrl + '/auth/employee/signup';

    return this.http.post<CompanyUser>(upCatalogRegisterUrl, item, httpOptions);
  }

  addManagerUser(item: CompanyUser) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })};

    const upCatalogRegisterUrl = environment.upCatalogApiUrl + '/auth/manager/signup';

    return this.http.post<CompanyUser>(upCatalogRegisterUrl, item, httpOptions);
  }

  updateCompanyUser(item: CompanyUser) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })};
    return this.http.put<CompanyUser>(this.upCatalogUrl, item, httpOptions);
  }

  // determine whether the user belongs or not to company users.
  isCompanyUser() {
    if (this.roles.includes(this.tokenStorageService.getUser().role.roleName)) {
      return true;
    }
    else {
      return false;
    }
  }

  isManager() {
    // slice the roles array to exclude employee
    // both manager and admin will return true
    if (this.roles.slice(1, 3).includes(this.tokenStorageService.getUser().role.roleName)) {
      return true;
    }
    else {
      return false;
    }
  }

  isAdmin() {
    // only admin will return true
    if (this.tokenStorageService.getUser().role.roleName === this.roles[2]) {
      return true;
    }
    else {
      return false;
    }
  }

  roleNameParser(role: string) {
    let name = role.split('_')[1];
    name = name.toLowerCase();
    name = name.charAt(0).toUpperCase() + name.slice(1);
    return name;
  }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CustomerNotification } from '../model/CustomerNotification';

@Injectable({
  providedIn: 'root'
})
export class CustomerNotificationService {
  upCatalogUrl = environment.upCatalogApiUrl + '/customer/notification';

// todo broaden with company notification system

  constructor(private http: HttpClient) { }

  getCustomerNotificationList() {
    return this.http.get<CustomerNotification[]>(this.upCatalogUrl);
  }

  sendCustomerNotification(notification: CustomerNotification) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })};

    console.log(notification.VATNumber)
    return this.http.post<CustomerNotification>(this.upCatalogUrl, notification, httpOptions)
  }

  updateCustomerNotification(id: number, result: boolean) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })};

    return this.http.put(this.upCatalogUrl + '/' + id, result, httpOptions)
  }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Category } from '../model/Category';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoryService { 
  upCatalogUrl = environment.upCatalogApiUrl + '/category';

  constructor(private http: HttpClient) { }

  getCategory() {
    return this.http.get<Category[]>(this.upCatalogUrl);
  }

  findCategoryById(id: number) {
    return this.http.get<Category>(this.upCatalogUrl + '/' + id);
  }

  addCategory(item: Category) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })};

    // return it in order to get a response
    return this.http.post<Category>(this.upCatalogUrl, item, httpOptions);
  }

  updateCategory(item: Category) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })};

    // return it in order to get a response
    return this.http.put<Category>(this.upCatalogUrl, item, httpOptions);
  }
}

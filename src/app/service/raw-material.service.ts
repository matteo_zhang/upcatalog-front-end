import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Category } from '../model/Category';
import { Subject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RawMaterial } from '../model/RawMaterial';

@Injectable({
  providedIn: 'root'
})
export class RawMaterialService {
  upCatalogUrl = environment.upCatalogApiUrl;

  private categoryListSource = new Subject<Category[]>();
  categoryList = this.categoryListSource.asObservable();

  constructor(private http: HttpClient) { }

  sendCategoryList(categoryList: Category[]) {
    this.categoryListSource.next(categoryList);
    console.log(categoryList);
  }

  getRawMaterials() {
    return this.http.get<RawMaterial[]>(this.upCatalogUrl + '/raw-material');
  }

  findRawMaterialById(id: number) {
    return this.http.get<RawMaterial>(this.upCatalogUrl + '/raw-material/' + id);
  }

  updateRawMaterialById(rawMaterial: RawMaterial) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })};

    return this.http.put<RawMaterial>(this.upCatalogUrl + '/raw-material/' + rawMaterial.id, rawMaterial, httpOptions);
  }

  addRawMaterial(rawMaterial: RawMaterial) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })};

    // return it in order to get a response
    return this.http.post<RawMaterial>(this.upCatalogUrl + '/raw-material', rawMaterial, httpOptions);
  }

  removeRawMaterial(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })};
      
    return this.http.delete<RawMaterial>(this.upCatalogUrl + '/raw-material/' + id, httpOptions);
  }

  deleteRawMaterial(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })};
      
    return this.http.delete<RawMaterial>(this.upCatalogUrl + '/raw-material/delete/' + id, httpOptions);
  }
}

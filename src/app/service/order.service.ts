import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Order } from '../model/Order';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  upCatalogUrl = environment.upCatalogApiUrl + '/customer/order';

  constructor(private http: HttpClient) { }
  
  getOrderList() {
    return this.http.get<Order[]>(this.upCatalogUrl);
  }

  findOrderById(id: number) {
    return this.http.get(this.upCatalogUrl + '/' + id);
  }

  addOrder(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })};

    // return it in order to get a response
    return this.http.post<Order>(this.upCatalogUrl, id, httpOptions);
  }
  
  payOrder(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })};

    // return it in order to get a response
    return this.http.put<Order>(this.upCatalogUrl, id, httpOptions);
  }
}

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { RawMaterialService } from './raw-material.service';

describe('RawMaterialListService', () => {
  beforeEach(async () => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ]
  }));

  let service: RawMaterialService;

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve raw material list of length 5 from GET', () => {
    // currently database has 5 raw mat
    const expectedLength = 5;

    service.getRawMaterials().subscribe(data => {
      expect(data.length).toBe(expectedLength);
    })
  });

  it('should send new raw material and receive back url by POST', () => {
    // currently database has 5 raw mat
    const dummyData = {
      "name":"Steel",
      "description":"Plain steel",
      "category":{
         "id":1,
         "name":"METAL"
      },
      "boughtPrice":250.0,
      "quantity":100,
      "minimumThreshold":30,
      "imagePath":null,
      "createdDate":"2020-10-26T15:19:46",
      "modifiedDate":"2020-10-26T15:19:46",
      "id":3,
      "supplier":{
         "name":"Steel Corp.",
         "address":"test",
         "telephoneNumber":123456789,
         "email":"test",
         "id":1,
         "vatnumber":"123456798"
      }
   }
    // service.sendData().subscribe( data => {

    // });
  });
});

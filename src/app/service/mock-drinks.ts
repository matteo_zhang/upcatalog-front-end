import { Drink } from '../model/Drink';

// Some fake data to test
const DRINKS: Drink[] =
[
  {
    name: 'Tea',
    quantity: 200,
    type: 'Hot'
  },
  {
    name: 'Espresso',
    quantity : 50,
    type : 'Hot'
  }
];

export { DRINKS };

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { ProductService } from './product.service';
import { environment } from 'src/environments/environment';


describe('ProductService', () => {
  let service: ProductService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProductService]
    });

    service = TestBed.get(ProductService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    // make sure no http request are invalid
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve product list of length 2 from GET', () => {
    // currently database has 2 products
    const expectedLength = 2;

    // test dummy data. Seems like the request.flush() uses this dummyData
    const dummyData = [
      {"name":"Rice Cooker","description":"Cook better with it!","category":{"id":3,"name":"KITCHEN"},"boughtPrice":15.0,"quantity":500,"minimumThreshold":10,"imagePath":null,"createdDate":null,"modifiedDate":null,"id":1,"serialNumber":"2r32452u","sellPrice":75.4,"quantityPerStock":30},{"name":"Cooking Pan","description":"Cook better with it!","category":{"id":3,"name":"KITCHEN"},"boughtPrice":5.0,"quantity":1000,"minimumThreshold":20,"imagePath":null,"createdDate":null,"modifiedDate":null,"id":8,"serialNumber":"asd232noi","sellPrice":60.0,"quantityPerStock":50}
    ]

    service.getProducts().subscribe(data => {
      expect(data.length).toBe(expectedLength);
      expect(data).toEqual(dummyData);
    });

    const request = httpMock.expectOne(environment.upCatalogApiUrl+'/product/all');
    // request should be done with GET method
    expect(request.request.method).toBe('GET');

    request.flush(dummyData);
  });
});

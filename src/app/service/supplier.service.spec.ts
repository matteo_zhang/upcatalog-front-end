import { TestBed } from '@angular/core/testing';

import { SupplierService } from './supplier.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('SupplierService', () => {
  beforeEach(async () => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ]
  }));

  it('should be created', () => {
    const service: SupplierService = TestBed.get(SupplierService);
    expect(service).toBeTruthy();
  });
});

import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { Company } from 'src/app/model/Company';
import { CompanyUser } from 'src/app/model/CompanyUser';
import { CustomerUser } from 'src/app/model/CustomerUser';
import { User } from 'src/app/model/User';
import { CompanyUserService } from 'src/app/service/company-user.service';
import { CompanyService } from 'src/app/service/company.service';
import { CustomerUserService } from 'src/app/service/customer-user.service';

@Component({
  selector: 'app-update-user-profile-form',
  templateUrl: './update-user-profile-form.component.html',
  styleUrls: ['./update-user-profile-form.component.sass']
})
export class UpdateUserProfileFormComponent implements OnChanges {
  uploadIcon = 'assets/img/upload-icon.svg';
  icon = 'assets/img/add-icon.svg';

  roles = ['ROLE_EMPLOYEE', 'ROLE_MANAGER', 'ROLE_ADMIN'];

  @Input()
  user: User;
  companyUser: CompanyUser;
  customerUser: CustomerUser;

  isUpdatingCompanyUser: boolean;

  isCompanyUser: boolean;
  isManagerUser: boolean;
  isAdminUser: boolean;

  companyList: Company[];

  constructor(private companyUserService: CompanyUserService,
    private customerUserService: CustomerUserService,
    private companyService: CompanyService) { }

  ngOnInit() {
    this.isManagerUser = this.companyUserService.isManager();
    this.isCompanyUser = this.companyUserService.isCompanyUser();
    this.isAdminUser = this.companyUserService.isAdmin();

    this.companyService.getCompanyUserList().subscribe(data => this.companyList = data);
  }

  // understand if it is a customer or company user
  ngOnChanges(changes: SimpleChanges) {
    if (this.roles.includes(this.user.role.roleName)) {
      this.isUpdatingCompanyUser = true;
      this.companyUser = this.user;
    }
  }

  onSubmit() {
    console.log(this.user);

    if(this.isUpdatingCompanyUser) {
        this.companyUserService.updateCompanyUser(this.user);
    }

  }
}

import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { CompanyUserService } from 'src/app/service/company-user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {
  logoIcon = 'assets/img/logo.svg';
  cartIcon = 'assets/img/cart.svg';
  orderIcon = 'assets/img/order-icon.svg';
  notificationIcon = 'assets/img/notification.svg';

  // user is company user, some feature will be visible
  isCompanyUser: boolean;

  // isAdminUser: boolean;

  constructor(private companyUserService: CompanyUserService) { }

  ngOnInit() {
    if (this.companyUserService.isCompanyUser()) {
      this.isCompanyUser = true;
    }
    else
      this.isCompanyUser = false;
  }
}

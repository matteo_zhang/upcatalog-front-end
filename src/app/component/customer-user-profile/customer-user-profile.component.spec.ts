import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerUserProfileComponent } from './customer-user-profile.component';

describe('CustomerUserProfileComponent', () => {
  let component: CustomerUserProfileComponent;
  let fixture: ComponentFixture<CustomerUserProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerUserProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerUserProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

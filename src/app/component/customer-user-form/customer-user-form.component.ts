import { Component, OnInit, Input } from '@angular/core';
import { CustomerUser } from 'src/app/model/CustomerUser';
import { CompanyService } from 'src/app/service/company.service';
import { CustomerUserService } from 'src/app/service/customer-user.service';
import { CompanyUserService } from 'src/app/service/company-user.service';
import { Company } from 'src/app/model/Company';

@Component({
  selector: 'app-customer-user-form',
  templateUrl: './customer-user-form.component.html',
  styleUrls: ['./customer-user-form.component.sass']
})
export class CustomerUserFormComponent implements OnInit {
  uploadIcon = 'assets/img/upload-icon.svg';
  icon = 'assets/img/add-icon.svg';

  companyList: Company[];

  @Input()
  user: CustomerUser;

  isCompanyUser: boolean;

  constructor(private companyUserService: CompanyUserService,
              private customerUserService: CustomerUserService,
              private companyService: CompanyService) { }

  ngOnInit() {
    this.isCompanyUser = this.companyUserService.isCompanyUser();

    this.companyService.getCompanyUserList().subscribe(
      data => {
        this.companyList = data;
        console.log("sad", this.companyList);
      }
    );
  }

  onSubmit() {
    console.log(this.user);

    this.customerUserService.updateCustomerUser(this.user).subscribe(data => {
      window.location.reload();
    });
  }

  parseToBooleanName(value: boolean) {
    if (value) {
      return "True";
    }
    else
      return "False";
  }
}


import { Component, OnInit } from '@angular/core';
import { Drink } from 'src/app/model/Drink';
import { DRINKS } from 'src/app/service/mock-drinks';
import { DrinksService } from 'src/app/service/drinks.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-drink-list',
  templateUrl: './drink-list.component.html',
  styleUrls: ['./drink-list.component.sass']
})
export class DrinkListComponent implements OnInit {
  drinks: Observable<Drink[]>;

  constructor(private drinkService: DrinksService) { }

  ngOnInit(): void {
    this.drinks = this.drinkService.getDrinks();
    // this.drinks.subscribe;
  }

}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ChatService } from 'src/app/service/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.sass']
})
export class ChatComponent implements OnDestroy {
  input;

  constructor(public chatService: ChatService) {}

  ngOnInit() {
    this.chatService.openConnection();
  }

  ngOnDestroy() {
  }

  closeChat() {
    const chat = document.getElementById('chat');
    chat.classList.add('hide');

    this.chatService.closeWebSocket();
  }

  showChat() {
    const chatBody = document.getElementById('chatBody');
    chatBody.classList.toggle('hide');
  }

  // enter key submits the message
  onSubmit(event) {
    if (event.keyCode === 13) {
      this.sendMessage();
    }
  }

  sendMessage() {
    if (this.input) {
      this.chatService.sendMessage(this.input);
      this.input = '';

      console.log(this.input)
    }
  }

}

import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Category } from 'src/app/model/Category';
import { ProductService } from 'src/app/service/product.service';
import { RawMaterialService } from 'src/app/service/raw-material.service';

@Component({
  selector: 'app-category-filter',
  templateUrl: './category-filter.component.html',
  styleUrls: ['./category-filter.component.sass']
})
export class CategoryFilterComponent implements OnInit {
  categoryList: Category[];

  @Output()
  selectedEvent = new EventEmitter();

  constructor(private productService: ProductService,
    private rawMaterialService: RawMaterialService) { }

  ngOnInit() {
    this.productService.categoryList.subscribe(category => this.categoryList = category);
    this.rawMaterialService.categoryList.subscribe(category => this.categoryList = category);
  }

  filterProductByCategory(data: Category) {
    this.selectedEvent.emit(data.name);
    console.log("filter", data)
    // alert('filter product call works with code(' + data + ')!');
  }
}
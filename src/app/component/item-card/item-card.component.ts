import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { Item } from '../../model/Item';
import { Router } from '@angular/router';
import { CartService } from 'src/app/service/cart.service';
import { CartElement } from 'src/app/model/CartElement';
import { Product } from 'src/app/model/Product';
import { RawMaterial } from 'src/app/model/RawMaterial';

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.sass']
})
export class ItemCardComponent implements OnChanges {

  @Input()
  item: Item = new Item();

  @Input()
  className: string;

  // 2 way binding with quantity input element.
  quantityInput: number = 1;

  // Typescript doesn't support well polymorphism (Item is superclass of both raw-material and product).
  // cast the received item to product whenever needed.
  product: Product;
  rawMaterial: RawMaterial;


  constructor(private route: Router,
              private cartService: CartService) { }

  ngOnInit() {
    this.cartService.getCartList().subscribe(data => {
      this.cartService.cart = data;
    });

    this.item = new Item();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.className == 'product') {
      this.product = this.item as Product;
    }
    else {
      this.rawMaterial = this.item as RawMaterial;
    }
  }

  addToCart(item: Product) {
    const el = new CartElement();
    el.product = item;
    el.quantity = this.quantityInput;

    if (el.quantity > 99) {
      alert('Input quantity exceed limit. (' + el.quantity + ')');
    }
    else {
      this.cartService.addCartElement(el).subscribe(
        success => console.log(success),
        error => console.error(error)
      );

      // alert('Added product: ' + item.name);
    }
  }

  onSelect(rawMaterial) {
    console.log(rawMaterial)
    this.route.navigate(['/add-raw-material', rawMaterial.id]);
  }
}

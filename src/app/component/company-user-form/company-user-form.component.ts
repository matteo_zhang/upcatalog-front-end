import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Company } from 'src/app/model/Company';
import { CompanyUser } from 'src/app/model/CompanyUser';
import { CompanyUserService } from 'src/app/service/company-user.service';
import { Role } from 'src/app/model/Role';
import { RoleService } from 'src/app/service/role.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-company-user-form',
  templateUrl: './company-user-form.component.html',
  styleUrls: ['./company-user-form.component.sass']
})
export class CompanyUserFormComponent implements OnInit {
  uploadIcon = 'assets/img/upload-icon.svg';
  icon = 'assets/img/add-icon.svg';

  companyList: Company[];
  roleList: Role[] = new Array();

  user: CompanyUser;

  isUpdatingCompanyUser: boolean;

  isCompanyUser: boolean;
  isManagerUser: boolean;
  isAdminUser: boolean;

  constructor(private companyUserService: CompanyUserService,
              private roleService: RoleService,
              private router: Router) { }

  ngOnInit() {
    this.isManagerUser = this.companyUserService.isManager();
    this.isCompanyUser = this.companyUserService.isCompanyUser();
    this.isAdminUser = this.companyUserService.isAdmin();

    this.roleService.getRoleList().subscribe(data => {
      this.roleList = data.slice(1, 3); // exclude the admin
    });

    this.user = new CompanyUser();
    this.user.enabled = true;
  }

  onSubmit() {
    console.log(this.user);

    // employee and manager will use different service method
    if (this.user.role.roleName === 'ROLE_EMPLOYEE') {
      this.companyUserService.addEmployeeUser(this.user).subscribe(
        data => {
        this.router.navigate(['/profile-list']);
       },
       error => console.error('error!', error)
      );
    }
    else {
      this.companyUserService.addManagerUser(this.user).subscribe(
        data => {
        this.router.navigate(['/profile-list']);
       },
       error => console.error('error!', error)
      );
    }
  }
}

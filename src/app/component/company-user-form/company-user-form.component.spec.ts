import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyUserFormComponent } from './company-user-form.component';

describe('CompanyUserFormComponent', () => {
  let component: CompanyUserFormComponent;
  let fixture: ComponentFixture<CompanyUserFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyUserFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyUserFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

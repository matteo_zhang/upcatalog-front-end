import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/model/Category';
import { CategoryService } from 'src/app/service/category.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.sass']
})
export class AddCategoryComponent implements OnInit {

  category: Category = new Category();
  isUpdating = false;
  pageTitle: string;

  constructor(private categoryService: CategoryService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    const id = parseInt(this.route.snapshot.paramMap.get('id'));

    if (!isNaN(id)) {
      this.categoryService.findCategoryById(id).subscribe(data => {
        this.category = data;
        console.log(this.category)
      });

      this.isUpdating = true;
      this.pageTitle = 'Modify Comapany User Profile';
    }
  }

  onSubmit() {
    if(this.isUpdating) {
      this.categoryService.updateCategory(this.category).subscribe(
        data => {
          window.location.reload();
        },
        error => console.log('error!', error)
      );
    }
    else {
      this.categoryService.addCategory(this.category).subscribe(
        data => {
          this.router.navigate(['/category']);          
        },
        error => console.log('error!', error)
      );
    }
  }
}

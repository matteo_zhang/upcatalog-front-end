import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/auth/service/token-storage.service';
import { CompanyUserService } from 'src/app/service/company-user.service';

@Component({
  selector: 'app-user-profile-button',
  templateUrl: './user-profile-button.component.html',
  styleUrls: ['./user-profile-button.component.sass']
})
export class UserProfileButtonComponent implements OnInit {
  userDefaultIcon = 'assets/img/user-default-icon.svg';
  arrow = 'assets/img/arrow.svg';

  isCompanyUser: boolean;

  constructor(private tokenService: TokenStorageService,
              private companyUserService: CompanyUserService
            ) { }

  ngOnInit() {
    this.isCompanyUser = this.companyUserService.isCompanyUser();
  }

  showDropDownMenu() {
    const dropDownMenu = document.getElementById('drop-down-menu');
    dropDownMenu.classList.toggle('hide');
  }

  logOut() {
    this.tokenService.signOut();
    window.location.reload();
  }
}

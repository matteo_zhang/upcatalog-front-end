import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateRawMaterialFormComponent } from './update-raw-material-form.component';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('UpdateRawMaterialFormComponent', () => {
  let component: UpdateRawMaterialFormComponent;
  let fixture: ComponentFixture<UpdateRawMaterialFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientTestingModule
      ],
      declarations: [ UpdateRawMaterialFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateRawMaterialFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

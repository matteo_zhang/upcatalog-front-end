import { Component, OnInit, Input, ɵConsole } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from 'src/app/model/Category';
import { Supplier } from 'src/app/model/Supplier';
import { RawMaterial } from 'src/app/model/RawMaterial';
import { RawMaterialService } from 'src/app/service/raw-material.service';
import { CategoryService } from 'src/app/service/category.service';
import { SupplierService } from 'src/app/service/supplier.service';
import { NotificationsService } from 'angular2-notifications';
import { CompanyUserService } from 'src/app/service/company-user.service';

@Component({
  selector: 'app-update-raw-material-form',
  templateUrl: './update-raw-material-form.component.html',
  styleUrls: ['./update-raw-material-form.component.sass']
})
export class UpdateRawMaterialFormComponent implements OnInit {
  uploadIcon = 'assets/img/upload-icon.svg';
  icon = 'assets/img/add-icon.svg';

  categoryList: Observable<Category[]>;
  supplierList: Observable<Supplier[]>;

  isAdmin: boolean;

  @Input()
  rawMaterial: RawMaterial;

  constructor(
    private rawMaterialService: RawMaterialService,
    // private toastService: NotificationsService,
    private categoryService: CategoryService,
    private supplierService: SupplierService,
    private companyUserService: CompanyUserService
  ) { }

  ngOnInit() {
    this.categoryList = this.categoryService.getCategory();
    this.supplierList = this.supplierService.getSupplier();

    this.isAdmin = this.companyUserService.isAdmin();
  }

  onSubmit() {
    // console.log("update raw material");
    console.log(this.rawMaterialService);
    this.rawMaterialService.updateRawMaterialById(this.rawMaterial).subscribe(
      data => {
        // this.toastService.success('Success!', 'Successfully Updated Raw Material!', {
        //   position: ['top', 'right'],
        //   timeOut: 2000,
        //   animation: 'fade',
        //   showProgressBar: false,
        //   maxStack: 4
        // });

        window.location.reload();
      },
      error => console.log('error!', error)
    );
  }

  deleteItem() {
    this.rawMaterialService.deleteRawMaterial(this.rawMaterial.id).subscribe(
      data => {
        console.log(data)
      },
      error => {
        console.error(error)
      }
    );
  }

  parseToBooleanName(value: boolean) {
    if (value) {
      return "True";
    }
    else
      return "False";
  }
}

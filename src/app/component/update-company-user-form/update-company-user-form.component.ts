import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Company } from 'src/app/model/Company';
import { CompanyUser } from 'src/app/model/CompanyUser';
import { CompanyUserService } from 'src/app/service/company-user.service';
import { CompanyService } from 'src/app/service/company.service';
import { Role } from 'src/app/model/Role';
import { RoleService } from 'src/app/service/role.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-update-company-user-form',
  templateUrl: './update-company-user-form.component.html',
  styleUrls: ['./update-company-user-form.component.sass']
})
export class UpdateCompanyUserFormComponent implements OnInit {
  uploadIcon = 'assets/img/upload-icon.svg';
  icon = 'assets/img/add-icon.svg';

  roleList: Role[] = new Array();

  @Input()
  user: CompanyUser;

  isCompanyUser: boolean;
  isManagerUser: boolean;
  isAdminUser: boolean;

  constructor(private companyUserService: CompanyUserService,
              private roleService: RoleService) { }

  ngOnInit() {
    this.isManagerUser = this.companyUserService.isManager();
    this.isCompanyUser = this.companyUserService.isCompanyUser();
    this.isAdminUser = this.companyUserService.isAdmin();

    this.roleService.getRoleList().subscribe(data => {
      this.roleList = data;
      this.roleList = this.roleList.slice(1, 3);
    });

    this.user = new CompanyUser();
  }

  onSubmit() {
    this.companyUserService.updateCompanyUser(this.user).subscribe(
      data => {
        window.location.reload();
      },
      error => {
        console.error('error!', error);
      }
    );
  }

  parseToBooleanName(value: boolean) {
    if (value) {
      return "True";
    }
    else
      return "False";
  }
}

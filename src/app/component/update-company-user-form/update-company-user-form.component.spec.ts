import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCompanyUserFormComponent } from './update-company-user-form.component';

describe('UpdateCompanyUserFormComponent', () => {
  let component: UpdateCompanyUserFormComponent;
  let fixture: ComponentFixture<UpdateCompanyUserFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCompanyUserFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCompanyUserFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

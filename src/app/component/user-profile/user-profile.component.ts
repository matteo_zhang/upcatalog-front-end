import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { User } from 'src/app/model/User';
import { CompanyUserService } from 'src/app/service/company-user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.sass']
})
export class UserProfileComponent implements OnChanges {
  parsedUserRoleName: string;
  editIcon = '/assets/img/edit-icon.svg';

  @Input()
  user: User = new User();

  constructor(private companyUserService: CompanyUserService,
              private router: Router) { }

  ngOnChanges() {
    console.log(this.user)
    this.parsedUserRoleName = this.companyUserService.roleNameParser(this.user.role.roleName);
  }

  modifyCustomerUser() {
    this.router.navigate(['/profile', 'customer', this.user.id]);
  }
}

import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Category } from 'src/app/model/Category';
import { RawMaterialService } from 'src/app/service/raw-material.service';
import { RawMaterial } from 'src/app/model/RawMaterial';

@Component({
  selector: 'app-raw-material-list',
  templateUrl: './raw-material-list.component.html',
  styleUrls: ['./raw-material-list.component.sass']
})
export class RawMaterialListComponent implements OnChanges {
  icon = '../../../assets/img/add-icon.svg';

  // filters
  @Input()
  public set categoryName(val: string) {
    // this.categoryName = val;
    console.log("asd")
    this.filterByCategoryName(val);
  }

  filteredRawMaterialList: RawMaterial[] = new Array();
  rawMaterialList: RawMaterial[] = new Array();
  categorylist: Category[] = new Array();

  selectedCategory: Category;

  constructor(private rawMaterialService: RawMaterialService) { }

  ngOnInit() {
    // Get products from java back end
    this.rawMaterialService.getRawMaterials().subscribe(rawMaterial => {
      // Get product list
      this.rawMaterialList = rawMaterial;
      this.filteredRawMaterialList = rawMaterial;
      
      // Get unique category list
      this.getUniqueCategoryList(this.rawMaterialList);

      // send category list to category filter component
      this.rawMaterialService.sendCategoryList(this.categorylist);
      // console.log(this.categorylist);
    });
  }

  ngOnChanges(OnChanges) {
    // console.log(this.categoryName)
  }

  filterByCategoryName(name) {
    // empty array
    this.filteredRawMaterialList = [];

    for (let i = 0; i < this.rawMaterialList.length; i++) {
      if (this.rawMaterialList[i].category.name === name) {
        this.filteredRawMaterialList.push(this.rawMaterialList[i]);
      }
    }
  }

  getUniqueCategoryList(rawMaterialList: Array<any>) {
    for (const rawMaterial of rawMaterialList) {
      if (!this.findCategory(rawMaterial.category)) {
        this.categorylist.push(rawMaterial.category);
      }
    }
  }

  findCategory(category: Category): boolean {
    for (const category2 of this.categorylist) {
      if (this.isEqual(category2, category)) {
        console.log('found');
        return true;
      }
    }

    console.log('not found');
    return false;
  }

  isEqual(obj1, obj2) {
    const obj1Keys = Object.keys(obj1); // ['id', 'name', 'etc..']
    const obj2Keys = Object.keys(obj2); // ['id', 'name', 'etc..']

    if (obj1Keys.length !== obj2Keys.length) {
      return false;
    }

    for (const objKey of obj1Keys) {
      if (obj1[objKey] !== obj2[objKey]) {
        return false;
      }
    }

    return true;
  }
}

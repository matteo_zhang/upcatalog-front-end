import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/service/product.service';
import { Product } from 'src/app/model/Product';
import { Category } from 'src/app/model/Category';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.sass']
})
export class ProductListComponent implements OnInit {
  productList: Product[] = new Array();
  categorylist: Category[] = new Array();

  constructor(private productService: ProductService) { }

  ngOnInit() {
    // Get products from java back end
    this.productService.getProducts().subscribe(product => {
      // Get product list
      this.productList = product;
      // Get unique category list
      this.getUniqueCategoryList(this.productList);

      // send category list to category filter component
      this.productService.sendCategoryList(this.categorylist);
    });
  }

  getUniqueCategoryList(productList: Array<any>) {
    for (const product of productList) {
      if (!this.findCategory(product.category)) {
        this.categorylist.push(product.category);
      }
    }
  }

  findCategory(category: Category): boolean {
    for (const category2 of this.categorylist) {
      if (this.isEqual(category2, category)) {
        return true;
      }
    }

    return false;
  }

  isEqual(obj1, obj2) {
    const obj1Keys = Object.keys(obj1); // ['id', 'name', 'etc..']
    const obj2Keys = Object.keys(obj2); // ['id', 'name', 'etc..']

    if (obj1Keys.length !== obj2Keys.length) {
      return false;
    }

    for (const objKey of obj1Keys) {
      if (obj1[objKey] !== obj2[objKey]) {
        return false;
      }
    }

    return true;
  }
}

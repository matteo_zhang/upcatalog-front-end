import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { RawMaterial } from 'src/app/model/RawMaterial';
import { RawMaterialService } from 'src/app/service/raw-material.service';
import { Category } from '../../model/Category';
import { Supplier } from '../../model/Supplier';
import { CategoryService } from '../../service/category.service';
import { SupplierService } from 'src/app/service/supplier.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-raw-material-form',
  templateUrl: './add-raw-material-form.component.html',
  styleUrls: ['./add-raw-material-form.component.sass']
})
export class AddRawMaterialFormComponent implements OnInit {
  uploadIcon = 'assets/img/upload-icon.svg';
  icon = 'assets/img/add-icon.svg';

  categoryList: Observable<Category[]>;
  supplierList: Observable<Supplier[]>;

  rawMaterial: RawMaterial = new RawMaterial();

  constructor(
    private rawMaterialService: RawMaterialService,
    private categoryService: CategoryService,
    private supplierService: SupplierService,
    private router: Router
  ) { }

  ngOnInit() {
    this.categoryList = this.categoryService.getCategory();
    this.supplierList = this.supplierService.getSupplier();
  }

  addTemplate() {
    alert('feature coming soon!');
  }

  onSubmit() {
    this.rawMaterialService.addRawMaterial(this.rawMaterial).subscribe(
      data => {
        // console.log('success!', data);
        this.router.navigate(['/raw-material']);
      },
      error => console.error('error!', error)
    );
  }
}

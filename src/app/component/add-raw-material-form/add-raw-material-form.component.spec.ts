import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BrowserModule, By } from '@angular/platform-browser';

import { AddRawMaterialFormComponent } from './add-raw-material-form.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AddRawMaterialFormComponent', () => {
  let component: AddRawMaterialFormComponent;
  let fixture: ComponentFixture<AddRawMaterialFormComponent>;
  let de: DebugElement;
  let el : HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientTestingModule
      ],
      declarations: [ AddRawMaterialFormComponent, AddRawMaterialFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRawMaterialFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css('form'));
    el = de.nativeElement;
  
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });

  it("should have as text 'add raw material form'", async(() => {
    expect(component.dummyText).toEqual('add raw material form');
  }));

  it('shuold call the onSubmit method', async(() => {
    fixture.detectChanges();
    // jasmine spy 
    spyOn(component, 'onSubmit');
    el = fixture.debugElement.query(By.css('button')).nativeElement;
    el.click();
    // we expect the button to be clicked 0 times since form is not valid
    expect(component.onSubmit).toHaveBeenCalledTimes(0);
  }));

  // Can't test with template-driven form
  // it('form should be invalid', async(() => {
    
  // }))

  // it('form should be valid', async(() => {
    
  // }))
});

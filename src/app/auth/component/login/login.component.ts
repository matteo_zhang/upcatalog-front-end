import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { User } from 'src/app/model/User';
import { AuthService } from 'src/app/auth/service/auth.service';
import { Router } from '@angular/router';
import { TokenStorageService } from '../../service/token-storage.service';
import { Role } from 'src/app/model/Role';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  loggedUser: User = new User();

  myForm: FormGroup;

  isLoggedIn = false;
  isLoginFailed = false;

  constructor(private fb: FormBuilder,
              private router: Router,
              private authService: AuthService,
              private tokenStorage: TokenStorageService
            ) { }


  ngOnInit() {
    // get token if exists
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.loggedUser.role = this.tokenStorage.getUser().role;
      if (this.loggedUser.role != null) {
        this.router.navigateByUrl('/product');
      }
    }

    this.myForm = this.fb.group({
      username: this.fb.control('johndoe', { validators: [
        Validators.required,
        Validators.minLength(3)
      ]}),
      password: this.fb.control('iamjohn', { validators: [
        Validators.required,
        Validators.minLength(3)
      ]})
    });
  }

  displayErrorMessage(errorMessageDiv: Element) {
    const loginFailed = document.createElement('p');

    loginFailed.innerText = 'Login Failed!';
    errorMessageDiv.innerHTML = ''; // remove previous created messages
    errorMessageDiv.classList.add('notification', 'is-light', 'is-danger');
    errorMessageDiv.appendChild(loginFailed);
  }

  onSubmit() {
    const errorMessageDiv = document.querySelector('#loginError');
    // if the form is invalid display error notification box
    if (!this.myForm.valid) {
      this.displayErrorMessage(errorMessageDiv);
    } else {
      errorMessageDiv.innerHTML = ''; // remove previous created messages

      // Authentication and get a JWT and user role
      const username = this.myForm.get('username').value;
      const password = this.myForm.get('password').value;

      this.authService.login({ username, password })
      .subscribe(data => {
        this.loggedUser.id = data.id; // seems to work 26/11/2020
        this.loggedUser.enabled = true; //TODO backend controller return enabled users
        const newRole = new Role();
        newRole.roleName = data.role;
        this.loggedUser.role = newRole;

        this.tokenStorage.saveToken(data.token);
        this.tokenStorage.saveUser(this.loggedUser);

        this.isLoginFailed = false;
        this.isLoggedIn = true;

        // console.log(this.tokenStorage.getUser());
        // this.reloadPage();

        this.router.navigateByUrl('/product');
      },
        err => {
          console.error(err);
          this.displayErrorMessage(errorMessageDiv);
          this.isLoginFailed = true;
        }
      );
    }
  }

  reloadPage() {
    window.location.reload();
  }
}

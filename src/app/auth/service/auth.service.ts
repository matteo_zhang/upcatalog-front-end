import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RawMaterial } from '../../model/RawMaterial';
import { Observable } from 'rxjs';
import { User } from 'src/app/model/User';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }

  login(credentials): Observable<any> {
    return this.http.post(environment.upCatalogApiUrl + '/auth/signin',
      credentials,
      httpOptions
    );
  }

  userBuilder() {

  }

  register(user) {
    return this.http.post(environment.upCatalogApiUrl + '/customer/auth/signin', {
      username: user.username,
      email: user.email,
      password: user.password
    }, httpOptions);
  }
}

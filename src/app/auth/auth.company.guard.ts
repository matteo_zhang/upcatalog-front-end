import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { User } from '../model/User';
import { CompanyUserService } from '../service/company-user.service';
import { TokenStorageService } from './service/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthCompanyGuard implements CanActivate {
  constructor(private router: Router,
              private companyUserService: CompanyUserService) { }
   
  // determine whether the user belongs or not to company users.
  canActivate(): boolean {
    if(this.companyUserService.isCompanyUser())
      return true;
    else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}

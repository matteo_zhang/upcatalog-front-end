import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './component/login/login.component';
import { AuthViewComponent } from './auth-view/auth-view.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { authInterceptorProviders } from './service/interceptor.service';
import { AuthGuard } from './auth.guard';
import { AuthCompanyGuard } from './auth.company.guard';

@NgModule({
  declarations: [
    LoginComponent,
    AuthViewComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule
  ],
  exports: [
    LoginComponent,
    AuthViewComponent
  ],
  providers: [authInterceptorProviders, AuthGuard, AuthCompanyGuard]
})
export class AuthModule { }

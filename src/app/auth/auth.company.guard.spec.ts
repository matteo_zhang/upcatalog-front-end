import { TestBed, async, inject } from '@angular/core/testing';

import { Auth.CompanyGuard } from './auth.company.guard';

describe('Auth.CompanyGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Auth.CompanyGuard]
    });
  });

  it('should ...', inject([Auth.CompanyGuard], (guard: Auth.CompanyGuard) => {
    expect(guard).toBeTruthy();
  }));
});

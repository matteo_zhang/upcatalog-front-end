import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DrinkListComponent } from './component/drink-list/drink-list.component';
import { HttpClientModule } from '@angular/common/http';
import { ProductViewComponent } from './pages/product-view/product-view.component';
import { HeaderComponent } from './component/header/header.component';
import { CategoryFilterComponent } from './component/category-filter/category-filter.component';
import { ProductListComponent } from './component/product-list/product-list.component';
import { FooterComponent } from './component/footer/footer.component';
import { RawMaterialViewComponent } from './pages/raw-material-view/raw-material-view.component';
import { RawMaterialListComponent } from './component/raw-material-list/raw-material-list.component';
import { AddRawMaterialComponent } from './pages/add-raw-material/add-raw-material.component';
import { AddRawMaterialFormComponent } from './component/add-raw-material-form/add-raw-material-form.component';
import { ItemCardComponent } from './component/item-card/item-card.component';
import { UpdateRawMaterialFormComponent } from './component/update-raw-material-form/update-raw-material-form.component';
import { AuthModule } from './auth/auth.module';
import { UserProfileComponent } from './component/user-profile/user-profile.component';
import { CustomerUserProfileComponent } from './component/customer-user-profile/customer-user-profile.component';
import { UserProfileButtonComponent } from './component/user-profile-button/user-profile-button.component';
import { SignupCustomerViewComponent } from './pages/signup-customer-view/signup-customer-view.component';
import { UserListViewComponent } from './pages/user-list-view/user-list-view.component';
import { NotificationViewComponent } from './pages/notification-view/notification-view.component';
import { CartViewComponent } from './pages/cart-view/cart-view.component';
import { ChatComponent } from './component/chat/chat.component';
import { UpdateUserProfileFormComponent } from './component/user-profile-form/update-user-profile-form.component';
import { CustomerUserFormComponent } from './component/customer-user-form/customer-user-form.component';
import { CompanyUserFormComponent } from './component/company-user-form/company-user-form.component';
import { UpdateCompanyUserProfileViewComponent } from './pages/update-company-user-profile-view/update-company-user-profile-view.component';
import { UserProfileViewComponent } from './pages/user-profile-view/user-profile-view.component';
import { UpdateCustomerUserProfileViewComponent } from './pages/update-customer-user-profile-view/update-customer-user-profile-view.component';
import { UpdateCompanyUserFormComponent } from './component/update-company-user-form/update-company-user-form.component';
import { OrderViewComponent } from './pages/order-view/order-view.component';
import { OrderListViewComponent } from './pages/order-list-view/order-list-view.component';
import { CategoryViewComponent } from './pages/category-view/category-view.component';
import { AddCategoryComponent } from './component/add-category/add-category.component';

@NgModule({
  declarations: [
    AppComponent,
    DrinkListComponent,
    ProductViewComponent,
    HeaderComponent,
    CategoryFilterComponent,
    ProductListComponent,
    FooterComponent,
    RawMaterialViewComponent,
    RawMaterialListComponent,
    AddRawMaterialComponent,
    AddRawMaterialFormComponent,
    ItemCardComponent,
    UpdateRawMaterialFormComponent,
    UserProfileComponent,
    CustomerUserProfileComponent,
    UserProfileButtonComponent,
    SignupCustomerViewComponent,
    UserListViewComponent,
    NotificationViewComponent,
    CartViewComponent,
    ChatComponent,
    UpdateUserProfileFormComponent,
    UpdateCompanyUserProfileViewComponent,
    CustomerUserFormComponent,
    CompanyUserFormComponent,
    UserProfileViewComponent,
    UpdateCustomerUserProfileViewComponent,
    UpdateCompanyUserFormComponent,
    OrderViewComponent,
    OrderListViewComponent,
    CategoryViewComponent,
    AddCategoryComponent
  ],
  imports: [
    AuthModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SimpleNotificationsModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

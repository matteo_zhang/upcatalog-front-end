import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductViewComponent } from './pages/product-view/product-view.component';
import { RawMaterialViewComponent } from './pages/raw-material-view/raw-material-view.component';
import { AddRawMaterialComponent } from './pages/add-raw-material/add-raw-material.component';
import { AuthGuard } from './auth/auth.guard';
import { SignupCustomerViewComponent } from './pages/signup-customer-view/signup-customer-view.component';
import { AuthCompanyGuard } from './auth/auth.company.guard';
import { UserListViewComponent } from './pages/user-list-view/user-list-view.component';
import { NotificationViewComponent } from './pages/notification-view/notification-view.component';
import { CartViewComponent } from './pages/cart-view/cart-view.component';
import { UpdateCompanyUserProfileViewComponent } from './pages/update-company-user-profile-view/update-company-user-profile-view.component';
import { UserProfileViewComponent } from './pages/user-profile-view/user-profile-view.component';
import { UpdateCustomerUserProfileViewComponent } from './pages/update-customer-user-profile-view/update-customer-user-profile-view.component';
import { OrderViewComponent } from './pages/order-view/order-view.component';
import { OrderListViewComponent } from './pages/order-list-view/order-list-view.component';
import { CategoryViewComponent } from './pages/category-view/category-view.component';
import { AddCategoryComponent } from './component/add-category/add-category.component';

const routes: Routes = [
  {
    path:'signup-customer',
    component: SignupCustomerViewComponent
  },
  {
    path: 'raw-material',
    component: RawMaterialViewComponent,
    canActivate: [
      AuthGuard,
      AuthCompanyGuard
    ]
  },
  {
    path: 'add-raw-material',
    component: AddRawMaterialComponent,
    canActivate: [
      AuthGuard,
      AuthCompanyGuard
    ]
  },
  {
    path: 'add-raw-material/:id',
    component: AddRawMaterialComponent,
    canActivate: [
      AuthGuard,
      AuthCompanyGuard
    ]
  },
  {
    path: 'product',
    component: ProductViewComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'category',
    component: CategoryViewComponent,
    canActivate: [
      AuthGuard,
      AuthCompanyGuard
    ]
  },
  {
    path: 'add-category',
    component: AddCategoryComponent,
    canActivate: [
      AuthGuard,
      AuthCompanyGuard
    ]
  },
  {
    path: 'add-category/:id',
    component: AddCategoryComponent,
    canActivate: [
      AuthGuard,
      AuthCompanyGuard
    ]
  },
  {
    path: 'company',
    component: ProductViewComponent,
    canActivate: [
      AuthGuard,
      AuthCompanyGuard
    ]
  },
  {
    path: 'myprofile',
    component: UserProfileViewComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'profile-list',
    component: UserListViewComponent,
    canActivate: [
      AuthGuard,
      AuthCompanyGuard
    ]
  },
  {
    path: 'profile/company',
    component: UpdateCompanyUserProfileViewComponent,
    canActivate: [
      AuthGuard,
      AuthCompanyGuard
    ]
  },
  {
    path: 'profile/company/:id',
    component: UpdateCompanyUserProfileViewComponent,
    canActivate: [
      AuthGuard,
      AuthCompanyGuard
    ]
  },
  {
    path: 'profile/customer/:id',
    component: UpdateCustomerUserProfileViewComponent,
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'notification',
    component: NotificationViewComponent,
    canActivate: [
      AuthGuard,
      AuthCompanyGuard
    ]
  },
  {
    path: 'cart',
    component: CartViewComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'order',
    component: OrderViewComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'order-list',
    component: OrderListViewComponent,
    canActivate: [
      AuthGuard,
      AuthCompanyGuard
    ]
  },
  {
    path: '**',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

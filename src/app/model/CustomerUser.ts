import { Company } from './Company';
import { User } from './User';

export class CustomerUser extends User {
    company: Company;
}

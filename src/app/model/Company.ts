export class Company {
  name: string;
  VATNumber: string;
  address: string;
  telephoneNumber: number;
  email: string;
}
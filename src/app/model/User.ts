import { Role } from './Role';

export class User {
  id: number;
  name: string;
  surname: string;
  username: string;
  password: string;
  email: string;
  enabled: boolean;
  role: Role;
  createdAt: string;
  modifiedAt: string;
}

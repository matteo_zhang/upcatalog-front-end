import { Category } from "./Category";

export class Item {
  id: number;
  name: string;
  description: string;
  imagePath: string;
  category: Category;
  boughtPrice: number;
  quantity: number;
  minimumThreshold: number;
  removed: boolean;
}
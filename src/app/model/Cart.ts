import { CustomerUser } from "./CustomerUser";

export class Cart {
    id: number;
    enabled: boolean;
    customerUser: CustomerUser;
    createdAt: string;
    updatedAt: string;
}
export class Drink {
  'name': string;
  'quantity': number;
  'type': string;
}

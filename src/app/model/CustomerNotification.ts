export class CustomerNotification {
    id: number;
    message: string;
    result: boolean;
    notificationType: NotificationType

    customerName: string;
    customerSurname: string;
    username: string;
    password: string;
    customerEmail: string;

    companyName: string;
    VATNumber: string;
    address: string;
    telephoneNumber: number;
    companyEmail: string;
}
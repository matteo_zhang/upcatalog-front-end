export class Supplier{
  id: number;
  name: string;
  vatnumber: string;
  address: string;
  telephoneNumber: number;
  email: string;
}

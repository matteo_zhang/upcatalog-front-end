import { Cart } from "./Cart";
import { CustomerUser } from "./CustomerUser";

export class Order {
    id: number;
    cart: Cart;
    customerUser: CustomerUser;
    paid: boolean;
    totalAmount: number;
    createdAt: string;
    updatedAt: string;
    paidAt: string;
}
import { CustomerUser } from "./CustomerUser";
import { Product } from "./Product";

export class CartElement {
    customerUser: CustomerUser
    product: Product;
    quantity: number;
}
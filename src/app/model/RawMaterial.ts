import { Category } from './Category';
import { Item } from './Item';
import { Supplier } from './Supplier';

export class RawMaterial extends Item {
  supplier: Supplier;
}

import { Item } from './Item';

export class Product extends Item {
  serialNumber: string;
  sellPrice: number;
  quantityPerStock: number;
}

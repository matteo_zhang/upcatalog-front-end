import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorageService } from 'src/app/auth/service/token-storage.service';
import { Order } from 'src/app/model/Order';
import { OrderService } from 'src/app/service/order.service';

@Component({
  selector: 'app-order-view',
  templateUrl: './order-view.component.html',
  styleUrls: ['./order-view.component.sass']
})
export class OrderViewComponent implements OnInit {
  icon = 'assets/img/add-icon.svg';

  orderList;

  private userId = this.tokenService.getUser().id;

  constructor(private tokenService: TokenStorageService,
              private orderService: OrderService,
              private router: Router
              ) { }

  ngOnInit() {
    this.orderService.findOrderById(this.userId).subscribe(
      data => {
        this.orderList = data;
      },
      error => {
        console.error(error)
      }
    );
  }

  // show product list belonging to the order
  showProductList(item: Order) {

  }
}

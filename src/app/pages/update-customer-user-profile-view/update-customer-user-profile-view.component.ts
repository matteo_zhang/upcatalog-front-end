import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompanyUserService } from 'src/app/service/company-user.service';
import { CustomerUserService } from 'src/app/service/customer-user.service';
import { CustomerUser } from 'src/app/model/CustomerUser';

@Component({
  selector: 'app-update-customer-user-profile-view',
  templateUrl: './update-customer-user-profile-view.component.html',
  styleUrls: ['./update-customer-user-profile-view.component.sass']
})
export class UpdateCustomerUserProfileViewComponent implements OnInit {
  customerUser: CustomerUser;

  constructor(private route: ActivatedRoute,
              private customerUserService: CustomerUserService) { }

  ngOnInit() {
    const id = parseInt(this.route.snapshot.paramMap.get('id'));

    if (!isNaN(id)) {
      this.customerUserService.getCustomerUserById(id).subscribe(data => {
        this.customerUser = data;
      });
    }
  }
}

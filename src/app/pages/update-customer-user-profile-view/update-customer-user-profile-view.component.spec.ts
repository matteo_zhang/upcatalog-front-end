import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCustomerUserProfileViewComponent } from './update-customer-user-profile-view.component';

describe('UpdateCustomerUserProfileViewComponent', () => {
  let component: UpdateCustomerUserProfileViewComponent;
  let fixture: ComponentFixture<UpdateCustomerUserProfileViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCustomerUserProfileViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCustomerUserProfileViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

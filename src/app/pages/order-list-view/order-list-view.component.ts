import { Component, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorageService } from 'src/app/auth/service/token-storage.service';
import { Order } from 'src/app/model/Order';
import { OrderService } from 'src/app/service/order.service';

@Component({
  selector: 'app-order-list-view',
  templateUrl: './order-list-view.component.html',
  styleUrls: ['./order-list-view.component.sass']
})
export class OrderListViewComponent implements OnInit {
  icon = 'assets/img/add-icon.svg';

  orderList: Order[];
  totalAmount = 0;
  totalCurrMonthAmount = 0;

  constructor(private orderService: OrderService,
              private router: Router) { }

  ngOnInit() {
    this.orderService.getOrderList().subscribe(
      data => {
        console.log(data);
        this.orderList = data;

        // calculate total order amount
        for (let i = 0; i < this.orderList.length; i++) {
          this.totalAmount += this.orderList[i].totalAmount;
        }
      },
      error => {
        console.error(error)
      }
    );
  }

  getCurrentMonthRevenue() {
    this.totalCurrMonthAmount = 0;
    const currMonth = new Date().getMonth();

    for (let i = 0; i < this.orderList.length; i++) {
      const date = new Date(this.orderList[i].paidAt);
      if (date.getMonth() === currMonth) {
        this.totalCurrMonthAmount += this.orderList[i].totalAmount;
      }
    }

    return this.totalCurrMonthAmount;
  }

  // show product list belonging to the order
  showProductList(item: Order) {

  }

  payOrder(item: Order) {
    this.orderService.payOrder(item.id).subscribe(
      data => {
        console.log(data);
        window.location.reload();
      },
      error => {
        console.error(error)
      }
    )
  }

}

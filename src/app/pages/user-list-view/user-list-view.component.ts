import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CompanyUser } from 'src/app/model/CompanyUser';
import { CustomerUser } from 'src/app/model/CustomerUser';
import { CompanyUserService } from 'src/app/service/company-user.service';
import { CustomerUserService } from 'src/app/service/customer-user.service';

@Component({
  selector: 'app-user-list-view',
  templateUrl: './user-list-view.component.html',
  styleUrls: ['./user-list-view.component.sass']
})
export class UserListViewComponent implements OnInit {
  icon = 'assets/img/add-icon.svg';

  companyUSers: CompanyUser[];
  customerUSers: CustomerUser[];

  isManager: boolean;

  constructor(private companyUserService: CompanyUserService,
              private customerUserService: CustomerUserService,
              private router: Router
            ) { }

  ngOnInit() {
    this.isManager = this.companyUserService.isManager();

    // // prevent employees to access user list page through the url
    // if (!this.isManager) {
    //   this.router.navigateByUrl('/product');
    // }

    this.companyUserService.getCompanyUserList().subscribe(data => {
      this.companyUSers = data;
    });

    this.customerUserService.getCustomerUsers().subscribe(data => {
      this.customerUSers = data;
    });
  }

  modifyCompanyUserProfile(item: CompanyUser) {
    this.router.navigate(['/profile', 'company', item.id]);
  }

  modifyCustomerUserProfile(item: CustomerUser) {
    this.router.navigate(['/profile', 'customer', item.id]);
  }

  roleNameParser(role: string): string {
    let name = role.split('_')[1];
    name = name.toLowerCase();
    name = name.charAt(0).toUpperCase() + name.slice(1);
    return name;
  }
}

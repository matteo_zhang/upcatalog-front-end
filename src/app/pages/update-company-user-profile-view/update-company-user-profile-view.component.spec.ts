import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCompanyUserProfileViewComponent } from './update-company-user-profile-view.component';

describe('UpdateCompanyUserProfileViewComponent', () => {
  let component: UpdateCompanyUserProfileViewComponent;
  let fixture: ComponentFixture<UpdateCompanyUserProfileViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCompanyUserProfileViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCompanyUserProfileViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompanyUser } from 'src/app/model/CompanyUser';
import { CompanyUserService } from 'src/app/service/company-user.service';
import { CustomerUserService } from 'src/app/service/customer-user.service';

@Component({
  selector: 'app-update-company-user-profile-view',
  templateUrl: './update-company-user-profile-view.component.html',
  styleUrls: ['./update-company-user-profile-view.component.sass']
})
export class UpdateCompanyUserProfileViewComponent implements OnInit {
  pageTitle: string;
  companyUser: CompanyUser;

  isUpdating = false;

  constructor(private route: ActivatedRoute,
              private companyUserService: CompanyUserService) {}

  ngOnInit() {
    const id = parseInt(this.route.snapshot.paramMap.get('id'));

    if (!isNaN(id)) {
      this.companyUserService.findCompanyUserById(id).subscribe(data => {
        this.companyUser = data;
      });

      this.isUpdating = true;
      this.pageTitle = 'Modify Comapany User Profile';
    }
    else {
      this.pageTitle = 'Create Comapany User Profile';
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductViewComponent } from './product-view.component';
import { HeaderComponent } from 'src/app/component/header/header.component';
import { ProductListComponent } from 'src/app/component/product-list/product-list.component';
import { FooterComponent } from 'src/app/component/footer/footer.component';
import { ItemCardComponent } from 'src/app/component/item-card/item-card.component';
import { CategoryFilterComponent } from 'src/app/component/category-filter/category-filter.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ProductViewComponent', () => {
  let component: ProductViewComponent;
  let fixture: ComponentFixture<ProductViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [ ProductViewComponent,
        HeaderComponent,
        ProductListComponent,
        ItemCardComponent,
        CategoryFilterComponent,
        FooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

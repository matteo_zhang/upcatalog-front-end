import { Component, OnInit } from '@angular/core';
import { HeaderComponent } from 'src/app/component/header/header.component';
import { CompanyUserService } from 'src/app/service/company-user.service';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.sass']
})
export class ProductViewComponent implements OnInit {
  icon = 'assets/img/add-icon.svg';
  
  isCompanyUser: boolean;

  constructor(private companyUserService: CompanyUserService) { }

  ngOnInit() {
    if (this.companyUserService.isManager())
      this.isCompanyUser = true;
    else
      this.isCompanyUser = false;
  }

}

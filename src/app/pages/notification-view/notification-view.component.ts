import { Component, OnInit } from '@angular/core';
import { CustomerNotification } from 'src/app/model/CustomerNotification';
import { CustomerNotificationService } from 'src/app/service/customer-notification.service';

@Component({
  selector: 'app-notification-view',
  templateUrl: './notification-view.component.html',
  styleUrls: ['./notification-view.component.sass']
})
export class NotificationViewComponent implements OnInit {
  customerNotifications: CustomerNotification[];

  constructor(private customerNotificationService: CustomerNotificationService) { }

  ngOnInit() {
    this.customerNotificationService.getCustomerNotificationList().subscribe(data => {
      this.customerNotifications = data;
    })
  }

  // add space on camelcase key name
  parseType(name: string): string {
    return name.replace(/([a-z])([A-Z])/g, `$1 $2`);
  }

  changeState(id: number, state: boolean) {
    if (state) {
      this.customerNotificationService.updateCustomerNotification(id, state).subscribe(
        result => console.log(result)
      );
    }
    else {
      this.customerNotificationService.updateCustomerNotification(id, state).subscribe(
        result => console.log(result)
      );
    }

    window.location.reload();
  }
}

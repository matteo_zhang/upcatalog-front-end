import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RawMaterial } from 'src/app/model/RawMaterial';
import { RawMaterialService } from 'src/app/service/raw-material.service';

@Component({
  selector: 'app-add-raw-material',
  templateUrl: './add-raw-material.component.html',
  styleUrls: ['./add-raw-material.component.sass']
})
export class AddRawMaterialComponent implements OnInit {
  addTitle = 'Add Raw Material';
  updateTitle = 'Update Raw Material';
  isUpdating = false;

  rawMaterial: RawMaterial = null;

  constructor(private route: ActivatedRoute, private rawMaterialService: RawMaterialService) { }

  ngOnInit() {
    const id = parseInt(this.route.snapshot.paramMap.get('id'));

    if (!isNaN(id)) {
      this.rawMaterialService.findRawMaterialById(id).subscribe(data => {
        this.rawMaterial = data;
      });

      this.isUpdating = true;
    }
  }
}

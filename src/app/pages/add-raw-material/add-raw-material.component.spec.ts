import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRawMaterialComponent } from './add-raw-material.component';
import { HeaderComponent } from 'src/app/component/header/header.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CategoryFilterComponent } from 'src/app/component/category-filter/category-filter.component';
import { AddRawMaterialFormComponent } from 'src/app/component/add-raw-material-form/add-raw-material-form.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UpdateRawMaterialFormComponent } from 'src/app/component/update-raw-material-form/update-raw-material-form.component';

describe('AddRawMaterialComponent', () => {
  let component: AddRawMaterialComponent;
  let fixture: ComponentFixture<AddRawMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        AddRawMaterialComponent,
        AddRawMaterialFormComponent,
        HeaderComponent,
        CategoryFilterComponent,
        UpdateRawMaterialFormComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRawMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupCustomerViewComponent } from './signup-customer-view.component';

describe('SignupCustomerViewComponent', () => {
  let component: SignupCustomerViewComponent;
  let fixture: ComponentFixture<SignupCustomerViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupCustomerViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupCustomerViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

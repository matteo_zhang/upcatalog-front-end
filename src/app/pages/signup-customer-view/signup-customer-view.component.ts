import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CustomerNotification } from 'src/app/model/CustomerNotification';
import { CustomerNotificationService } from 'src/app/service/customer-notification.service';

@Component({
  selector: 'app-signup-customer-view',
  templateUrl: './signup-customer-view.component.html',
  styleUrls: ['./signup-customer-view.component.sass']
})
export class SignupCustomerViewComponent implements OnInit {
  myForm: FormGroup;

  constructor(private fb: FormBuilder,
              private customerNotificationService: CustomerNotificationService) { }

  ngOnInit() {
    this.myForm = this.fb.group({
      customerName: this.fb.control('jackie', { validators: [
        Validators.required,
        Validators.minLength(3)
      ]}),
      surname: this.fb.control('freezy', { validators: [
        Validators.required,
        Validators.minLength(3)
      ]}),
      username: this.fb.control('jackiefreezy', { validators: [
        Validators.required,
        Validators.minLength(3)
      ]}),
      password: this.fb.control('iamjackie', { validators: [
        Validators.required,
        Validators.minLength(3)
      ]}),
      customerEmail: this.fb.control('jackie@company.com', { validators: [
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
      ]}),

      companyName: this.fb.control('jackie&co', { validators: [
        Validators.required,
        Validators.minLength(3)
      ]}),
      VATNumber: this.fb.control('123456789', { validators: [
        Validators.required,
        Validators.minLength(3)
      ]}),
      address: this.fb.control('street avenue', { validators: [
        Validators.required,
        Validators.minLength(3)
      ]}),
      telephoneNumber: this.fb.control('123456789', { validators: [
        Validators.required,
        Validators.minLength(3)
      ]}),
      companyEmail: this.fb.control('jackie@company.com', { validators: [
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
      ]}),
    });

  }

  onSubmit() {
    let notification = new CustomerNotification();

    notification.customerName = this.myForm.get('customerName').value;
    notification.customerSurname = this.myForm.get('surname').value;
    notification.username = this.myForm.get('username').value;
    notification.password = this.myForm.get('password').value;
    notification.customerEmail = this.myForm.get('customerEmail').value;

    notification.companyName = this.myForm.get('companyName').value;
    notification.companyEmail = this.myForm.get('companyEmail').value;
    notification.VATNumber = this.myForm.get('VATNumber').value;
    notification.address = this.myForm.get('address').value;
    notification.telephoneNumber = this.myForm.get('telephoneNumber').value;
    console.log(notification);
    // this.customerNotificationService.sendCustomerNotification(notification).subscribe(
    //   response => console.log("Success", response),
    //   error => console.error("error", error)
    // )
  }

}

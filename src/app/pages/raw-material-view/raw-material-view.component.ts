import { Component, OnInit } from '@angular/core';
import { CompanyUserService } from 'src/app/service/company-user.service';

@Component({
  selector: 'app-raw-material-view',
  templateUrl: './raw-material-view.component.html',
  styleUrls: ['./raw-material-view.component.sass']
})
export class RawMaterialViewComponent implements OnInit {
  icon = 'assets/img/add-icon.svg';

  // filter variables
  categoryName;

  isCompanyUser: boolean;

  constructor(private companyUserService: CompanyUserService) { }

  ngOnInit() {
    this.isCompanyUser = this.companyUserService.isCompanyUser();
  }

  filterByCategoryName(name) {
    this.categoryName = name;
  }
}

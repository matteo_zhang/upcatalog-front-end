import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RawMaterialViewComponent } from './raw-material-view.component';
import { HeaderComponent } from 'src/app/component/header/header.component';
import { CategoryFilterComponent } from 'src/app/component/category-filter/category-filter.component';
import { RawMaterialListComponent } from 'src/app/component/raw-material-list/raw-material-list.component';
import { FooterComponent } from 'src/app/component/footer/footer.component';
import { ItemCardComponent } from 'src/app/component/item-card/item-card.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('RawMaterialViewComponent', () => {
  let component: RawMaterialViewComponent;
  let fixture: ComponentFixture<RawMaterialViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [ RawMaterialViewComponent,
        HeaderComponent,
        CategoryFilterComponent,
        RawMaterialListComponent,
        ItemCardComponent,
        FooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RawMaterialViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

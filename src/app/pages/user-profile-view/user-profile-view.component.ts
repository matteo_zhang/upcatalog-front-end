import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/User';
import { CustomerUserService } from 'src/app/service/customer-user.service';
import { TokenStorageService } from 'src/app/auth/service/token-storage.service';
import { CompanyUserService } from 'src/app/service/company-user.service';

@Component({
  selector: 'app-user-profile-view',
  templateUrl: './user-profile-view.component.html',
  styleUrls: ['./user-profile-view.component.sass']
})
export class UserProfileViewComponent implements OnInit {
  user: User;


  constructor(private customerUserService: CustomerUserService,
              private companyUserService: CompanyUserService,
              private tokenStorage: TokenStorageService) { }

  ngOnInit() {
    // get user role
    const loginUser: User = this.tokenStorage.getUser();
    const id = loginUser.id;
    console.log("asd", loginUser)

    switch (loginUser.role.roleName) {
      case 'ROLE_CUSTOMER':
        this.customerUserService.getCustomerUserById(id).subscribe(data => {
          this.user = data;
        });
        break;

      case 'ROLE_EMPLOYEE':
        this.companyUserService.findCompanyUserById(id).subscribe(data => {
          this.user = data;
          console.log("asd", loginUser)
        });
        break;

      case 'ROLE_MANAGER':
        this.companyUserService.findCompanyUserById(id).subscribe(data => {
          this.user = data;
        });
        break;

      case 'ROLE_ADMIN':
        this.companyUserService.findCompanyUserById(id).subscribe(data => {
          this.user = data;
        });
        break;
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { TokenStorageService } from 'src/app/auth/service/token-storage.service';
import { CartElement } from 'src/app/model/CartElement';
import { Product } from 'src/app/model/Product';
import { CartService } from 'src/app/service/cart.service';
import { OrderService } from 'src/app/service/order.service';

@Component({
  selector: 'app-cart-view',
  templateUrl: './cart-view.component.html',
  styleUrls: ['./cart-view.component.sass']
})
export class CartViewComponent implements OnInit {
  totalAmount = 0;

  constructor(private cartService: CartService,
              private orderService: OrderService,
              private tokenService: TokenStorageService) { }

  ngOnInit() {
    this.cartService.getCartList().subscribe(data => {
      this.cartService.cart = data;

      // calculate total order amount
      for (let i = 0; i < this.cartService.cart.length; i++) {
        this.totalAmount += this.cartService.cart[i].product.boughtPrice * this.cartService.cart[i].quantity;
      }
    });
  }

  confirmOrder() {
    const userId = this.tokenService.getUser().id;
    this.orderService.addOrder(userId).subscribe(
      () => {
        // this.toastService.success('Success!', 'Successfully Updated Raw Material!', {
        //   position: ['top', 'right'],
        //   timeOut: 2000,
        //   animation: 'fade',
        //   showProgressBar: false,
        //   maxStack: 4
        // });
        alert('Order made successfully!');
        window.location.reload();
      }
    )
  }

  emptyCart() {
    this.cartService.emptyCart();
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Category } from 'src/app/model/Category';
import { CategoryService } from 'src/app/service/category.service';

@Component({
  selector: 'app-category-view',
  templateUrl: './category-view.component.html',
  styleUrls: ['./category-view.component.sass']
})
export class CategoryViewComponent implements OnInit {
  icon = 'assets/img/add-icon.svg';

  categorylist: Category[] = new Array();

  constructor(private categoryService: CategoryService,private route: Router) { }

  ngOnInit() {
    this.categoryService.getCategory().subscribe(
      data => {
        this.categorylist = data
        console.log(data)
      }
    );
  }

  modifyCategory(item: Category) {
    this.route.navigate(['/add-category', item.id]);
  }
}
